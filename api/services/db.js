var mongo =			require ('mongodb').MongoClient
var ObjectId =		require ('mongodb').ObjectId
var log =			require ('../services/log')
var db =			null

function cb (err, result, type, callback) {
	if (process.env.NODE_VERBOSE == 'true') {
		if (err) {
			log (`db`, `[${type.toUpperCase ()}][ERROR] ${err}`)
		} else {
			log ('db', `[${type.toUpperCase ()}][SUCCESS]`)
		}
	}
	if (callback) {
		callback (err, result)
	}
}

module.exports =	{
	connect: function (options, callback) {
		if (!options || typeof options != 'object') {
			return log ('error', `db.connect (options[, callback]). You need to specify |options|.`)
		}

		mongo.connect (`mongodb://${options.user ? options.user : ''}${options.user || options.password ? ':' : ''}${options.password ? options.password : ''}${options.user || options.password ? '@' : ''}${options.host}:${options.port ? options.port : ''}/${options.name ? options.name : 'test'}`, function (err, database) {
			if (!err && database) {
				db = database
			}

			if (err) {
				return log ('error', err, callback)
			}
			return cb (err, database, 'connect', callback)
		})
	},

	insert: function (options, callback) {
		if (!options || typeof options != 'object' || !options.object || typeof options.object != 'object' || !options.collection) {
			return log ('error', `db.insert (options[, callback]). You need to specify |options|.`, callback)
		} else if (!db) {
			return log ('error', `You must use db.connect () first.`, callback)
		} else if (!db.collection (options.collection)) {
			return log ('error', `Collection [${options.collection}] doesn't exists.`, callback)
		}

		db.collection (options.collection).insert (options.object, function (err, result) {
			cb (err, result, 'insert', callback)
		})
	},

	find: function (options, callback) {
		if (!options || !options.collection) {
			return log ('error', `db.find (options[, callback]). You need to specify |options|.`, callback)
		} else if (!db) {
			return log ('error', `You must use db.connect () first.`, callback)
		} else if (!db.collection (options.collection)) {
			return log ('error', `Collection [${options.collection}] doesn't exists.`, callback)
		}

		if (typeof options.query == 'string') {
			db.collection (options.collection).find ({
				_id: new require ('mongodb').ObjectId (options.query),
			}, options.fields).toArray (function (err, result) {
				cb (err, result, 'find', callback)
			})
		} else {
			db.collection (options.collection).find (options.query, options.fields).toArray (function (err, result) {
				cb (err, result, 'find', callback)
			})
		}
	},

	update: function (options, callback) {
		if (!options || !options.collection) {
			return log ('error', `db.find (options[, callback]). You need to specify |options|.`, callback)
		} else if (!db) {
			return log ('error', `You must use db.connect () first.`, callback)
		} else if (!db.collection (options.collection)) {
			return log ('error', `Collection [${options.collection}] doesn't exists.`, callback)
		}

		if (typeof options.query == 'string') {
			db.collection (options.collection).updateOne ({
				_id: new require ('mongodb').ObjectId (options.query),
			}, {
				$set: options.newValues
			}, {
				raw: true,
			}, function (err, result) {
				cb (err, result, 'update', callback)
			})
		} else {
			db.collection (options.collection)[options.onlyOne ? 'updateOne' : 'updateMany'] (options.query, {
				$set: options.newValues
			}, {
				raw: true
			}, function (err, result) {
				cb (err, result, 'update', callback)
			})
		}
	},

	delete: function (options, callback) {
		if (!options || !options.collection) {
			return log ('error', `db.delete (options[, callback]). You need to specify |options|.`, callback)
		} else if (!db) {
			return log ('error', `You must use db.connect () first.`, callback)
		} else if (!db.collection (options.collection)) {
			return log ('error', `Collection [${options.collection}] doesn't exists.`, callback)
		}

		if (typeof options.query == 'string') {
			db.collection (options.collection).deleteOne ({
				_id: new require ('mongodb').ObjectId (options.query),
			}, function (err, result) {
				cb (err, result, 'delete', callback)
			})
		} else {
			if (!options.query) {
				if (options.deleteAll === true) {
					return db.collection (options.collection).deleteMany ({ }, function (err, result) {
						cb (err, result, 'delete', callback)
					})
				}
				return cb ('options.query is empty. To delete all rows, set options.deleteAll = true.', {}, 'delete', callback)
			}
			return db.collection (options.collection)[options.onlyOne ? 'deleteOne' : 'deleteMany'] (options.query, function (err, result) {
				cb (err, result, 'delete', callback)
			})
		}
	},

	database: function () {
		return db
	},
}

/*

db.connect (options[, callback])
db.connect ({
	user: 'toto',
	password: 'toto',
	host: '127.0.0.1',
	port: '27017',
	name: 'test'
}, function (database) {

})

################################################################################

db.insert (options[, callback])
db.insert ({
	collection: 'test',
	object: {
		user: 'toto',
		pwd: 'toto'
	}
}, function (err, result) {

})

################################################################################

db.find (options[, callback])
db.find ({
	collection: 'test',
	query: <id>,
	fields: {
		pseudo: 1,
		password: 0
	}
})
db.find ({
	collection: 'test',
	query: {
		pseudo: 'toto'
	},
	fields: {}
}, function (err, docs) {

})

################################################################################

db.update (options[, callback])
db.update ({
	collection: 'test',
	query: <id>,
	newValues: {
		pseudo: 'newPwd',
	}
})
db.update ({
	collection: 'test',
	query: {
		pseudo: 'toto'
	},
	newValues: {
		password: 'newPwd'
	},
	[onlyOne: true]				// default false
}, function (err, docs) {

})

################################################################################

db.delete (options[, callback])
db.delete ({
	collection: collection,
	query: <id>,
})
db.delete ({
	collection: collection,
	[onlyOne: true,]			// default false
	[deleteAll: true,]			// default false
	query: {
		pseudo: 'toto'
	},
}, function (err, docs) {

})

*/
