require ('colors')
var fs =		require ('fs')
var moment =	require ('moment')
var path =		`${__dirname}/../../logs`

module.exports = function (type, data, req, callback) {
	if (req && typeof req == 'function') {
		callback = req
		req = null
	}
	var date = moment ().format ('YYYY-MM-DD HH:mm:ss\'ssss')
	var pseudo = req && req.session && req.session.pseudo ? req.session.userInfos.pseudo : 'unknown user'
	var msg = `[${date}][${get_ip (req)}][${pseudo}][${type}] -> ${data}`.replace ('SUCCESS', 'SUCCESS'.green).replace ('ERROR', 'ERROR'.red)

	console[type == 'error' ? 'error' : 'log'] (msg)

	if (!fs.existsSync (`${path}`)) {
		fs.mkdirSync (`${path}`)
	}

	fs.appendFile (`${path}/${type}.log`, `${msg}\n`)

	if (callback) {
		callback (data)
	}

	return data
}

function get_ip (req) {
	if (!req) {
		return ''
	} else if (req.headers['x-forwarded-for']) {
		return req.headers['x-forwarded-for']
	} else if (req.connection && req.connection.remoteAddress) {
		return req.connection.remoteAddress
	} else if (req.socket && req.socket.remoteAddress) {
		return req.socket.remoteAddress
	} else if (req.connection && req.connection.socket && req.connection.socket.remoteAddress) {
		return req.connection.socket.remoteAddress
	}
	return 'unknown ip'
}

/*

require ('log') ('error', 'an error has occurred', req | null, function (err) {

})

*/
