var Signalement =	require ('../models/signalement')
var mail =			require ('../services/mail')
var infos =			require ('../../config/infos')

function generateMapLink (address) {
	return `<a href='http://maps.google.com/maps?q=${encodeURIComponent (address)}' target='_blank'>${address}</a>`
}

function getMailOfBrigade (brigade) {
	for (var i in infos.brigades) {
		if (infos.brigades[i].nom == brigade) {
			return infos.brigades[i].mail
		}
	}
	return null
}

function sendMailToBrigade (brigade, data) {
	var brigadeMail = getMailOfBrigade (brigade)

	if (!brigadeMail || brigadeMail.length <= 0) {
		return
	}
	mail.send ({
		from: '"SPA Helper" <helper@gmail.com>',
		to: brigadeMail,
		subject: 'Nouvelle mission',
		html:  `<p>Bonjour,</p>
				<p>Votre équipe a été assigné à une nouvelle intervention.</p>
				<p>Il s'agit d'un(e) <b>${data.animal}</b> de couleur <b>${data.couleur}</b>, <b>${data.collier} collier</b>. Il a été aperçu près de <b>${generateMapLink (data.adresse)}</b>. Son état est <b>${data.etat}</b>.</p>`
	}, function (err, infos) {
		if (err) {
			console.log (err)
		} else {
			console.log (`Mail sent to brigade ${brigade} [${brigadeMail}]`)
		}
	})
}

module.exports =	{
	create: function (req, res) {
		if (!req || !req.body) {
			return res.end ('false')
		}

		if (req.body.date && /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test (req.body.date)) {
			var dateParts = req.body.date.split ('-')
			req.body.date = `${dateParts[2]}/${dateParts[1]}/${dateParts[0].substring (2, 4)}`
		}
		if (req.body.creneau && /^[0-9]{2}:[0-9]{2}$/.test (req.body.creneau)) {
			var hour = parseInt (req.body.creneau.split (':')[0])
			req.body.creneau = `${hour}h-${hour + 1}h`
		}



		Signalement.create (req.body, function (err, created) {
			if (err || created.insertedCount != 1) {
				return res.end ()
			}

			if (process.env.NODE_ENV == 'production' && infos.admin.mail) {
				mail.send ({
					from: '"SPA Helper" <helper@gmail.com>',
					to: infos.admin.mail,
					subject: 'Nouveau signalement',
					html: `<p>Bonjour ${infos.admin.nom}</p><p>Un nouveau signalement a été mis en ligne.</p><p>Il s'agit d'un(e) <b>${req.body.animal.toLowerCase ()}</b> de couleur <b>${req.body.couleur}</b>, <b>${req.body.collier.toString () == 'true' ? 'avec un' : 'sans'} collier</b>. Il a été aperçu près de <b>${generateMapLink (req.body.adresse)}</b>. Son état est <b>${req.body.etat.toLowerCase ()}</b>.</p>`
				}, function (err) {
					if (err) {
						console.log (err)
					} else {
						console.log (`Mail send to the admin [${infos.admin.nom} <${infos.admin.mail}>]`)
					}
				})
			}

			if (req.body.brigade && req.body.brigade != 'null' ) {
				sendMailToBrigade (req.body.brigade, {
					animal: req.body.animal ? req.body.animal.toLowerCase () : false || found[0].animal ? found[0].animal.toLowerCase () : 'animal',
					couleur: req.body.couleur,
					collier: req.body.collier.toString () == 'true' ? 'avec un' : 'sans',
					adresse: req.body.adresse,
					etat: req.body.etat.toLowerCase ()
				})
			}
			return res.end (created.insertedIds[0].toString ())
		})

	},

	read: function (req, res) {
		var query = {}
		var fields = {}

		if (typeof req.params.id != 'undefined') {
			if (Object.keys (infos).indexOf (req.params.id) >= 0 && typeof req.params.key != 'undefined') {
				query[req.params.id] = req.params.key
				req.params.key = req.params.key2
			} else if (req.params.id.length == 24) {
				query = req.params.id
			}
		}
		if (typeof req.params.key != 'undefined') {
			fields['_id'] = 0
			fields[req.params.key] = 1
		}
		Signalement.find ({
			query: query,
			fields: fields
		}, function (err, docs) {
			return res.json (query == req.params.id ? docs[0] : docs)
		})
	},

	update: function (req, res) {
		var id = req.body._id
		delete req.body._id

		Signalement.find ({
			query: id
		}, function (err, found) {
			if (err || !found) {
				return res.end ()
			}

			if (found[0].brigade && found[0].brigade != 'null') {
				delete req.body.brigade
			}

			Signalement.update ({
				onlyOne: true,
				query: id.toString (),
				newValues: req.body
			}, function (err, modified) {
				if (err || modified.modifiedCount != 1) {
					return res.end ()
				}

				if ((!found[0].brigade || found[0].brigade == 'null') && req.body.brigade) {
					sendMailToBrigade (req.body.brigade, {
						animal: req.body.animal ? req.body.animal.toLowerCase () : false || found[0].animal ? found[0].animal.toLowerCase () : 'animal',
						couleur: req.body.couleur,
						collier: req.body.collier.toString () == 'true' ? 'avec un' : 'sans',
						adresse: req.body.adresse,
						etat: req.body.etat.toLowerCase ()
					})
				}

				if (['Sauvé', 'Echec'].indexOf (found[0].status) < 0 && ['Sauvé', 'Echec'].indexOf (req.body.status) >= 0) {
					if (req.body.alerteur || found[0].alerteur) {
						var message = req.body.status == 'Echec' ?	'mais n\'ont malheureusement rien pu faire pour l\'animal. Nous sommes désolés.' :
																	'et ont réussi à sauver l\'animal !</p><p>Merci pour votre aide ! 🐶'
						mail.send ({
							from: '"SPA Helper" <helper@gmail.com>',
							to: req.body.alerteur || found[0].alerteur,
							subject: 'Fin de la mission',
							html: `<p>Bonjour,</p><p>Vous nous avez recement signalé un(e) <b>${req.body.animal ? req.body.animal.toLowerCase () : false || found[0].animal ? found[0].animal.toLowerCase () : 'animal'}</b>. Nos équipes sont intervenues ${message}</p>`
						}, function (err, infos) {
							if (err) {
								console.log (err)
							} else {
								console.log (`Mail send to alerteur ${req.body.alerteur || found[0].alerteur}: ${req.body.status}`)
							}
						})
					}
				}

				return res.end (id.toString ())
			})
		})
	},

	delete: function (req, res) {

		res.end ('delete')
	}
}
