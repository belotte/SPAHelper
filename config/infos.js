module.exports = {
	admin: {
		nom: 'Tim',
		mail: 'tim@hellozack.fr'
	},
	status: [{
		nom: 'Signalé',
		message: 'Une alerte a été faite et le signalement bient d\'apparaitre sur le dashboard',
	},
	{
		nom: 'Assigné',
		message: 'Une brigade a été assignée au signalement',
	},
	{
		nom: 'Sauvé',
		message: 'La brigade est intervenue et a sauvé l\'animal',
	},
	{
		nom: 'Echec',
		message: 'La brigade est intervenue et n\'a pas sauvé l\'animal',
	},
	{
		nom: 'Annulé',
		message: 'Signalment annulé par l\'operateur'
	}],
	brigades: [{
		nom: '75015-A',
		mail: 'brigade_75015_A@gmail.com'
	}, {
		nom: '75015-B',
		mail: ''
	}, {
		nom: '75002',
		mail: ''
	}, {
		nom: '75001',
		mail: ''
	}, {
		nom: '75006-C',
		mail: ''
	}],
	etats: ['Très faible', 'Faible', 'Moyen', 'Bon'],
	animaux: ['Chat', 'Chien', 'Lapin', 'Perroquet'],
}
