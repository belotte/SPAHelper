/*---------------------------REQUIREMENTS-------------------------------------*/
require ('colors')
var express =		require ('express')
var app =			express ()
var http =			require ('http').Server (app)
var router =		express.Router ()
var controllers =	{
	infos:			require ('./api/controllers/infos'),
	signalement:	require ('./api/controllers/signalement'),
}
var bodyParser =	require ('body-parser')
var mail =			require ('./api/services/mail')
var utils =			require ('./config/utils')
var dbConfig =		require ('./config/db')
var mailConfig =	require ('./config/mail')
/*----------------------------------------------------------------------------*/

/*---------------------------MIDDLEWARES--------------------------------------*/
app.set ('view engine', 'pug')
app.use (bodyParser.urlencoded ({
	extended: false
}))
app.use ('/js',		express.static ('assets/js'))
app.use ('/css',	express.static ('assets/css'))
app.use ('/icons',	express.static ('assets/icons'))
app.use ('/images',	express.static ('assets/images'))
app.use ('/', router)
/*----------------------------------------------------------------------------*/

require ('./api/services/db').connect ({
	user:		dbConfig.user,
	password:	dbConfig.password,
	host:		dbConfig.host,
	port:		dbConfig.port,
	name:		dbConfig.name
}, function (err) {
	if (err) {
		return console.error (err)
	}

	/*-----------------------SERVICES-----------------------------------------*/
	mail.init ({
		user:		mailConfig.user,
		password:	mailConfig.password
	})
	/*--------------------------------------------------------------------*/

	/*-----------------------ROUTE--------------------------------------------*/
	require ('./routes/index')			(router, controllers)
	require ('./routes/infos')			(router, controllers)
	require ('./routes/signalement')	(router, controllers)
	/*------------------------------------------------------------------------*/

	/*-----------------------CONFIG-------------------------------------------*/
	/*------------------------------------------------------------------------*/

	app.listen (utils.port, function () {
		console.log (`Server running on port ${utils.port.toString ().cyan}.`)
		console.log (`Environment: ${process.env.NODE_ENV ? process.env.NODE_ENV.toUpperCase ().blue : 'DEVELOPMENT'.green}.`)
	})
})
