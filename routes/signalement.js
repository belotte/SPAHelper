module.exports = function (router, controllers) {
	router.route ('/api/v1/signalements/:id?/:key?/:key2?')
	.post (controllers.signalement.create)
	.get (controllers.signalement.read)
	.put (controllers.signalement.update)
	.delete (controllers.signalement.delete)
}
