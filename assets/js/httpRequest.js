function arrayToQuery (array) {
	var query = ''

	for (var key in array) {
		if (array.hasOwnProperty(key)) {
			if (query != '')
			query += '&'
			query += key + '=' + array[key]
		}
	}
	return query
}

function postRequest (path, args, callback) {
	var xhr = new XMLHttpRequest ()

	if (!callback && typeof args == 'function') {
		callback = args
		args = {}
	}

	xhr.open ('POST', path, true)
	xhr.setRequestHeader ('Content-Type', 'application/x-www-form-urlencoded')
	xhr.send (arrayToQuery (args))

	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			if (callback) {
				callback (xhr.responseText)
			}
		}
		if (xhr.readyState == 4) {
			delete xhr
		}
	}
}

function putRequest (path, args, callback) {
	var xhr = new XMLHttpRequest ()

	if (!callback && typeof args == 'function') {
		callback = args
		args = {}
	}

	xhr.open ('PUT', path, true)
	xhr.setRequestHeader ('Content-Type', 'application/x-www-form-urlencoded')
	xhr.send (arrayToQuery (args))

	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			if (callback) {
				callback (xhr.responseText)
			}
		}
		if (xhr.readyState == 4) {
			delete xhr
		}
	}
}

function deleteRequest (path, args, callback) {
	var xhr = new XMLHttpRequest ()

	if (!callback && typeof args == 'function') {
		callback = args
		args = {}
	}

	xhr.open ('DELETE', path, true)
	xhr.setRequestHeader ('Content-Type', 'application/x-www-form-urlencoded')
	xhr.send (arrayToQuery (args))

	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			if (callback) {
				return callback (xhr.responseText)
			}
		}
		if (xhr.readyState == 4) {
			delete xhr
		}
	}
}

function getRequest (path, args, callback) {
	var xhr = new XMLHttpRequest ()

	if (!callback && typeof args == 'function') {
		callback = args
		args = {}
	}

	xhr.open ('GET', path + '?' + arrayToQuery (args), true)
	xhr.send (null)

	xhr.onreadystatechange = function () {
		if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
			if (callback) {
				callback (xhr.responseText)
			}
		}
		if (xhr.readyState == 4) {
			delete xhr
		}
	}
}

/*
	getRequest (path[, {
		var1: value,
		var2: value
	}[, function (response) {
		console.log (response)
	}]])

	putRequest (path[, {
		var1: value,
		var2: value
	}[, function (response) {
		console.log (response)
	}]])

	postRequest (path[, {
		var1: value,
		var2: value
	}[, function (response) {
		console.log (response)
	}]])

	deleteRequest (path[, {
		var1: value,
		var2: value
	}[, function (response) {
		console.log (response)
	}]])
*/
