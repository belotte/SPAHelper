$ (document).ready (function () {
	$ ('input[name = date]').val (moment ().format ('YYYY-MM-DD'))
	$ ('input[name = creneau]').val (moment ().format ('HH:mm'))

	getRequest ('/api/v1/signalements', function (data) {
		data = JSON.parse (data).reverse ()

		for (var i in data) {
			insert (data[i])
		}
		sort ()
	})

	$ ('select[name = brigade]').change (function () {
		if ($ (this).val () && $ (this).val () != 'none' && $ ('select[name = status]').val () == 'Signalé') {
			$ ('select[name = status]').val ('Assigné')
		} else if ($ ('select[name = status]').val () == 'Assigné') {
			$ ('select[name = status]').val ('Signalé')
		}
	})

	$ ('input[name = adresse]').on ('change', function () {
		addMarker ($ ('input[name = adresse]').val ())
	})
})

var brigades = null
var etats = null
var status = null
var savedData = null

getRequest ('/api/v1/infos/etats', function (data) {
	etats = data ? JSON.parse (data) : []

	$ ('select[name = etat]').append (getEtatsOptions ())
})

getRequest ('/api/v1/infos/status', function (data) {
	delete status
	status = JSON.parse (data)

	$ ('select[name = status]').append (getStatusOptions ())
})

getRequest ('/api/v1/infos/brigades', function (data) {
	brigades = data ? JSON.parse (data) : []

	$ ('select[name = brigade]').append (getBrigadesOptions ())
})

function getEtatsOptions (selected) {
	var rows = ''
	for (var i in etats) {
		rows += `<option value = '${etats[i]}' ${etats[i] == selected ? 'selected' : ''}>${etats[i]}</option>`
	}
	return rows
}

function getStatusOptions (selected) {
	var rows = ''
	for (var i in status) {
		rows += `<option value = '${status[i].nom}' ${status[i].nom == selected ? 'selected' : ''}>${status[i].nom}</option>`
	}
	return rows
}

function getBrigadesOptions (selected) {
	var rows = `<option value = 'none'>-</option>`
	for (var i in brigades) {
		rows += `<option value = '${brigades[i].nom}' ${brigades[i].nom == selected ? 'selected' : ''}>${brigades[i].nom}</option>`
	}
	return rows
}

function create (object) {
	var data = {
		status: object.children[0].children[0].value,
		date: object.children[1].children[0].value,
		creneau: object.children[2].children[0].value,
		alerteur: object.children[3].children[0].value,
		adresse: object.children[4].children[0].value,
		animal: object.children[5].children[0].value,
		couleur: object.children[6].children[0].value,
		etat: etats[object.children[7].children[0].selectedIndex],
		collier: object.children[8].children[0].checked,
		brigade: object.children[9].children[0].selectedIndex > 0 ? brigades[object.children[9].children[0].selectedIndex - 1].nom : null,
	}

	if (data.date) {
		var dateParts = data.date.split ('-')
		data.date = `${dateParts[2]}/${dateParts[1]}/${dateParts[0].substring (2, 4)}`
	}

	if (data.creneau) {
		var hour = parseInt (data.creneau.split (':')[0])
		data.creneau = `${hour.toString ().length == 1 ? '0' : ''}${hour}h-${(hour + 1).toString ().length == 1 ? '0' : ''}${hour + 1}h`
	}

	postRequest ('/api/v1/signalements', data, function (response) {
		if (response.length > 10) {
			data._id = response
			insert (data)
			cancel ()
		}
	})
}

function fillRow (data) {
	return `<td class = '${data.status.substring (0, 2)}'>${data.status}</td>
	<td>${data.date}</td>
	<td>${data.creneau}</td>
	<td>${data.alerteur}</td>
	<td>${data.adresse}</td>
	<td>${data.animal}</td>
	<td>${data.couleur}</td>
	<td>${data.etat}</td>
	<td>${data.collier && data.collier.toString () == 'true' ? 'oui' : 'non'}</td>
	<td>${data.brigade && data.brigade != 'null' ? data.brigade : '-'}</td>
	${['Sauvé', 'Echec', 'Annulé'].indexOf(data.status) < 0 ? '<td><input type = button value = "Sauvé" onclick = "updateStatus (this.parentElement.parentElement, \'Sauvé\')"></td><td><input type = button value = "Echec" onclick = "updateStatus (this.parentElement.parentElement, \'Echec\')"></td>' : ''}`
}

function insert (data) {
	var row = 	`<tr id = '${data._id}' onclick = 'update (this)'>${fillRow (data)}</tr>`

	$ ('div.card#signalements table').append (row)
	sort ()
}

function update (object) {
	if (event.target.tagName == 'INPUT') {
		return
	}

	var data = {
		_id: object.id,
		status: object.children[0].innerHTML,
		date: object.children[1].innerHTML,
		creneau: object.children[2].innerHTML,
		alerteur: object.children[3].innerHTML,
		adresse: object.children[4].innerHTML,
		animal: object.children[5].innerHTML,
		couleur: object.children[6].innerHTML,
		etat: object.children[7].innerHTML,
		collier: object.children[8].innerHTML,
		brigade: object.children[9].innerHTML
	}

	$ ('.updating').attr ('class', '')
	object.className = 'updating'

	if (data.date) {
		var dateParts = data.date.split ('/')
		data.date = `${parseInt (dateParts[2]) > new Date ().getFullYear () % 100 ? '19' : '20'}${dateParts[2]}-${dateParts[1]}-${dateParts[0]}`
	}

	if (data.creneau) {
		data.creneau = `${data.creneau.split ('h')[0]}:00`
	}

	$ ('div.card#create tr:nth-child(2)').attr ('id', data._id)
	$ ('div.card#create input[name = date]').val (data.date)
	$ ('div.card#create input[name = creneau]').val (data.creneau)
	$ ('div.card#create input[name = alerteur]').val (data.alerteur)
	$ ('div.card#create input[name = adresse]').val (data.adresse)
	$ ('div.card#create input[name = animal]').val (data.animal)
	$ ('div.card#create input[name = couleur]').val (data.couleur)
	$ ('div.card#create input[name = collier]').attr ('checked', data.collier == 'oui')
	$ ('div.card#create select[name = etat]').html (getEtatsOptions (data.etat))
	$ ('div.card#create select[name = brigade]').html (getBrigadesOptions (data.brigade))
	$ ('div.card#create select[name = brigade]').removeAttr ('disabled')
	if (data.brigade && data.brigade != '-') {
		$ ('div.card#create select[name = brigade]').attr ('disabled', 'disabled')
	}
	$ ('div.card#create select[name = status]').html (getStatusOptions (data.status))
	$ ('div.card#create input[type = submit]').attr ('value', 'Modifier')
	$ ('div.card#create input[type = submit]').attr ('onclick', 'sendUpdate (this.parentElement.parentElement)')
	if ($ ('div.card#create table tr:nth-child(2) input[value = Annuler]').length == 0) {
		$ ('div.card#create table tr:nth-child(2)').append (`<td><input type = button onclick = 'cancel ()' value = Annuler></td>`)
	}

	addMarker (data.adresse)
}

function sendUpdate (object) {
	var data = {
		_id: object.id,
		status: object.children[0].children[0].value,
		date: object.children[1].children[0].value,
		creneau: object.children[2].children[0].value,
		alerteur: object.children[3].children[0].value,
		adresse: object.children[4].children[0].value,
		animal: object.children[5].children[0].value,
		couleur: object.children[6].children[0].value,
		etat: etats[object.children[7].children[0].selectedIndex],
		collier: object.children[8].children[0].checked,
		brigade: object.children[9].children[0].selectedIndex > 0 ? brigades[object.children[9].children[0].selectedIndex - 1].nom : null,
	}

	object.id = ''

	if (data.date) {
		var dateParts = data.date.split ('-')
		data.date = `${dateParts[2]}/${dateParts[1]}/${dateParts[0].substring (2, 4)}`
	}

	if (data.creneau) {
		var hour = parseInt (data.creneau.split (':')[0])
		data.creneau = `${hour.toString ().length == 1 ? '0' : ''}${hour}h-${(hour + 1).toString ().length == 1 ? '0' : ''}${hour + 1}h`
	}

	putRequest ('/api/v1/signalements', data, function (response) {
		if (response.length > 10) {
			data._id = response

			$ (`#${data._id}`).html (fillRow (data))
			sort ()
		}
		cancel (object)
	})
}

function cancel () {
	var row = `<td><select name = 'status' required></td>
	<td><input type = 'date' name = 'date' required></td>
	<td><input type = 'time' name = 'creneau' required></td>
	<td><input type = 'email' name = 'alerteur' required></td>
	<td><input type = 'text' name = 'adresse' required></td>
	<td><input type = 'text' name = 'animal' required></td>
	<td><input type = 'text' name = 'couleur' required></td>
	<td><select name = 'etat' required></td>
	<td><input type = 'checkbox' id = 'c1' name = 'collier'><label for = 'c1'></td>
	<td><select name = 'brigade'></td>
	<td><input type = 'submit' onclick = 'create (this.parentElement.parentElement)' value = 'Creer'></td>`

	$ ('div.card#create tr:nth-child(2)').html (row)
	$ ('select[name = etat]').append (getEtatsOptions ())
	$ ('select[name = brigade]').append (getBrigadesOptions ())
	$ ('select[name = status]').append (getStatusOptions ())
	$ ('input[name = date]').val (moment ().format ('YYYY-MM-DD'))
	$ ('input[name = creneau]').val (moment ().format ('HH:mm'))
	$ ('div.card#create select[name = brigade]').removeProp ('disabled')
	$ ('input[name = adresse]').on ('change', function () {
		addMarker ($ ('input[name = adresse]').val ())
	})
	updateCharts ()
}

function sort () {
	var rows = $ ('div.card#signalements table tr:nth-child(n+2)')
	var data = {}
	for (var i in status) {
		data[status[i].nom] = []
	}
	for (var row in rows) {
		if (row == 'length' || row >= rows.length) {
			break
		}

		var dateParts = rows[row].children[1].innerHTML.split ('/')

		data[rows[row].children[0].innerHTML].push ({
			id: row,
			status: rows[row].children[0].innerHTML,
			date: new Date (`${parseInt (dateParts[2]) > new Date ().getFullYear () % 100 ? '19' : '20'}${dateParts[2]}-${dateParts[1]}-${dateParts[0]} ${rows[row].children[2].innerHTML.split ('h')[0]}:00`),
			tr: rows[row]
		})
	}
	$ ('div.card#signalements table tr:nth-child(n+2)').remove ()
	for (var i in data) {
		data[i] = data[i].sort (function (a,b) {
			return new Date (a.date).getTime () < new Date (b.date).getTime () ? 1 : -1
		})
		for (var k in data[i]) {
			$ ('div.card#signalements table').append (data[i][k].tr)
		}
	}
}

function updateStatus (object, type) {
	object.children[0].innerHTML = type
	object.children[0].className = type.substring (0, 2)
	$ (object.children[11]).remove ()
	$ (object.children[10]).remove ()

	putRequest ('/api/v1/signalements', {
		_id: object.id,
		status: type
	}, function (response) {
		if (response.length > 10) {
			sort ()
			updateCharts ()
		}
	})
}
